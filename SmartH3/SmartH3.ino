
#include <SoftwareSerial.h>

//pins
#define grow A2
#define pinWater A0 //Anya 
#define Volume 3 //Sasha
#define voice 4


int door_pin = 2; //Lera

//qrow
const int minGrow = 200;
const int maxGrow = 700;
int countGrow = 0;
int countGrow2 = 0;

//Water
int maxWater = 800;

bool checkVolume = false;     //Sasha
bool checkDoor = false;
bool check = false; //
int readVolume = 0;

//card
SoftwareSerial rfid(7, 8); // RX, TX
boolean alarmOn=false;
boolean pos=false;
int read_vol=0;
int command=0;

void setup() {
  pinMode(A2, INPUT);
  Serial.begin(9600);
  pinMode(pinWater, INPUT);
  rfid.begin(9600);
}

void loop() {
  Water ();
  DOOR ();
  MotionSensor();
  WetGrow();
  Card();
}
void OutputAlarm(){
  Serial.println("ALARM");
}

void Card()
{
  command=Serial.read();
  if(command=='0'){
    alarmOn = false;
    Serial.println("Deactivated by port");

  }
  if (rfid.available())
  {
    if(rfid.find("74002738A5CE")) cardAcces(1);
  }
  if(alarmOn)read_vol = digitalRead(Volume);
  if(read_vol>0 && !pos && alarmOn)
  {
    OutputAlarm();
    pos = true;

  }
  else if(read_vol == 0 && pos){
      pos=false;

  }
  if(pos && alarmOn)
    Music();
}
void cardAcces(int n){
       Serial.print("Card read : ");
       Serial.println(n);
      if(alarmOn)
      {
      Serial.println("Deactivated by card");
      alarmOn=false;
      tone(voice,500);
      delay(100);
      tone(voice,1000);
      delay(100);
      noTone(voice);
      }
      else
      {
      Serial.println("Activated");
      alarmOn=true;
      tone(voice,1000);
      delay(100);
      tone(voice,500);
      delay(100);
      noTone(voice); 
      delay(1500);
      }
     
}
void Music(){
  tone(voice, 1000);
  delay(100);
  tone(voice, 4600);
  delay(100);
  tone(voice, 900);
  delay(100);
  noTone(voice);
}

void MotionSensor() {
  readVolume = digitalRead(Volume);
  if (readVolume > 0 && !checkVolume) {
    OutputMotionSensor();
    checkVolume = true;
  }
  else if (readVolume == 0 && checkVolume) {
    checkVolume = false;
  }
}

void OutputMotionSensor() {
  Serial.println("dvk");
}

void DOOR () {
  int valDoor = digitalRead(door_pin);

  if (valDoor == HIGH && !checkDoor) {
    checkDoor = true;
    Serial.println("dc");
  }
  else if (valDoor == LOW && checkDoor) {
    checkDoor = false;
    Serial.println("do");
  }
}

void Water() {
  int W_val = analogRead(pinWater);
  if (W_val < maxWater && !check) {
    Serial.println("pvv");
    check = true;
  }
  else if (W_val > maxWater && (check)) {
    check = false;
  }
}

void WetGrow() {
  int valGrow = analogRead(grow);
  if (valGrow <= minGrow && countGrow2 == 1)
  {
    Serial.println("dvn");
    countGrow2 = 0;
  }
  else if (valGrow >= maxGrow && countGrow == 0 && countGrow2 == 0) {
    Serial.println("dvc");
    countGrow = 1;
    countGrow2 = 1;
  }
  else if ((valGrow >= minGrow) && (valGrow <= maxGrow) && countGrow == 1) {
    Serial.println("dvn");
    countGrow = 0;
  }
}




