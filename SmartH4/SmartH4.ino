
#include <SoftwareSerial.h>

//pins
#define grow A2
#define pinWater A0 //Anya 
#define Volume 3 //Sasha
#define voice 4
#define LedKitchen 9
#define LedHoll 10
#define LedBathRoom 11
#define LedLivingRoom 12

int door_pin = 2; //Lera

//qrow
const int minGrow = 200;
const int maxGrow = 700;
int countGrow = 0;
int countGrow2 = 0;

//Water
int maxWater = 800;

bool checkVolume = false;     //Sasha
bool checkDoor = false;
bool check = false; //
int readVolume = 0;

//card
SoftwareSerial rfid(7, 8); // RX, TX
boolean alarmOn = false;
boolean pos = false;
int read_vol = 0;
int command = 0;
int DiodState = 0;
void setup() {
  pinMode(A2, INPUT);
  Serial.begin(9600);
  pinMode(pinWater, INPUT);
  rfid.begin(9600);
}

void loop() {
  Water ();
  DOOR ();
  MotionSensor();
  WetGrow();
  Card();
  Light();

}
void OutputAlarm() {
  Serial.println("ALARM");
}

void Card()
{
  command = Serial.read();
  if (command == 'F' or command == "0") {
    alarmOn = false;
    Serial.println("Deactivated by port");

  }

  if (command == 'T' or command == "1") {
    alarmOn = true;
    Serial.println("Activated by port");

  }
  if (rfid.available())
  {
    if (rfid.find("74002738A5CE")) cardAcces(1);
  }
  if (alarmOn)read_vol = digitalRead(Volume);
  if (read_vol > 0 && !pos && alarmOn)
  {
    OutputAlarm();
    pos = true;

  }
  else if (read_vol == 0 && pos) {
    pos = false;

  }
  if (pos && alarmOn)
    Music();
}
void cardAcces(int n) {
  Serial.print("Card read : ");
  Serial.println(n);
  if (alarmOn)
  {
    Serial.println("Deactivated by card");
    Serial.println("od");
    alarmOn = false;
    tone(voice, 500);
    delay(100);
    tone(voice, 1000);
    delay(100);
    noTone(voice);
  }
  else
  {
    Serial.println("Activated");
    Serial.println("oa");
    alarmOn = true;
    tone(voice, 1000);
    delay(100);
    tone(voice, 500);
    delay(100);
    noTone(voice);
    delay(1500);
  }

}
void Music() {
  tone(voice, 1000);
  delay(100);
  tone(voice, 4600);
  delay(100);
  tone(voice, 900);
  delay(100);
  noTone(voice);
}


void MotionSensor() {
  readVolume = digitalRead(Volume);
  if (readVolume > 0 && !checkVolume) {
    OutputMotionSensor();
    checkVolume = true;
  }
  else if (readVolume == 0 && checkVolume) {
    checkVolume = false;
  }
}

void OutputMotionSensor() {
  Serial.println("dvk");
}

void DOOR () {
  int valDoor = digitalRead(door_pin);

  if (valDoor == HIGH && !checkDoor) {
    checkDoor = true;
    Serial.println("dc");
  }
  else if (valDoor == LOW && checkDoor) {
    checkDoor = false;
    Serial.println("do");
  }
}

void Water() {
  int W_val = analogRead(pinWater);
  if (W_val < maxWater && !check) {
    Serial.println("pvv");
    check = true;
    Titanic();
  }
  else if (W_val > maxWater && (check)) {
    check = false;
  }
}

void WetGrow() {
  int valGrow = analogRead(grow);
  if (valGrow <= minGrow && countGrow2 == 1)
  {
    Serial.println("dvn");
    countGrow2 = 0;
  }
  else if (valGrow >= maxGrow && countGrow == 0 && countGrow2 == 0) {
    Serial.println("dvc");
    countGrow = 1;
    countGrow2 = 1;
  }
  else if ((valGrow >= minGrow) && (valGrow <= maxGrow) && countGrow == 1) {
    Serial.println("dvn");
    countGrow = 0;
  }
}

void Light()
{ DiodState = Serial.read();

  if (DiodState == 'A') {
    digitalWrite(LedKitchen, HIGH);
  }
  else if (DiodState == 'Z') {
    digitalWrite(LedKitchen, LOW);
  }


  else if (DiodState == 'S') {
    digitalWrite(LedHoll, HIGH);
  }
  else if (DiodState == 'X') {
    digitalWrite(LedHoll, LOW);
  }

  else if (DiodState == 'D') {
    digitalWrite(LedBathRoom, HIGH);
  }
  else if (DiodState == 'C') {
    digitalWrite(LedBathRoom, LOW);
  }

  else if (DiodState == 'G') {
    digitalWrite(LedLivingRoom, HIGH);
  }
  else if (DiodState == 'B') {
    digitalWrite(LedLivingRoom, LOW);
  }
}

void Titanic(){
  oktava_4_do(voice, 800);
  delay(900);
  oktava_4_do(voice, 200);
  delay(300);
  oktava_4_do(voice, 400);
  delay(500);
  oktava_4_do(voice, 400);
  delay(500);
  oktava_3_ci(voice, 400);//800
  delay(500);
  oktava_4_do(voice, 800);
  delay(900);
  oktava_4_do(voice, 400);
  delay(500);
  oktava_3_ci(voice, 400);
  delay(500);
  oktava_4_do(voice, 800);
  delay(900);
  oktava_4_re(voice, 400);//400
  delay(500);
  oktava_4_mi(voice, 900);
  delay(1000);
  oktava_4_re(voice, 800);
  delay(900);
  oktava_4_do(voice, 800);
  delay(900);
  oktava_4_do(voice, 200);
  delay(300);
  oktava_4_do(voice, 400);
  delay(500);
  oktava_4_do(voice, 400);
  delay(500);
  oktava_3_ci(voice, 400);
  delay(500);
  oktava_4_do(voice, 800);
  delay(900);
  oktava_4_do(voice, 400);
  delay(500);
  oktava_3_sol(voice, 2000);
  delay(2100);


  oktava_4_do(voice, 800);
  delay(900);
  oktava_4_do(voice, 200);
  delay(300);
  oktava_4_do(voice, 400);
  delay(500);
  oktava_4_do(voice, 400);
  delay(500);
  oktava_3_ci(voice, 400);//800
  delay(500);
  oktava_4_do(voice, 800);
  delay(900);
  oktava_4_do(voice, 400);
  delay(500);
  oktava_3_ci(voice, 400);
  delay(500);
  oktava_4_do(voice, 800);
  delay(900);
  oktava_4_re(voice, 400);
  delay(500);
  oktava_4_mi(voice, 800);
  delay(900);
  oktava_4_re(voice, 800);
  delay(500);
  oktava_4_do(voice, 800);
  delay(900);
  oktava_4_do(voice, 200);
  delay(300);
  oktava_4_do(voice, 400);
  delay(500);
  oktava_4_do(voice, 400);
  delay(500);
  oktava_3_ci(voice, 400);
  delay(500);
  oktava_4_do(voice, 800);
  delay(900);
  oktava_4_do(voice, 400);
  delay(500);
  oktava_3_sol(voice, 3000);
  delay(3100);


  oktava_3_lya(voice, 800);
  delay(900);
  oktava_3_ci(voice, 800);
  delay(900);
  oktava_4_do(voice, 2000);
  delay(2100);
  oktava_4_re(voice, 1200);
  delay(1300);
  oktava_3_sol(voice, 400);
  delay(500);
  oktava_4_sol(voice, 800);
  delay(900);
  oktava_4_fa(voice, 400);
  delay(500);
  oktava_4_mi(voice, 200);
  delay(300);
  oktava_4_re(voice, 1200);
  delay(1300);
  oktava_4_mi(voice, 800);
  delay(900);
  oktava_4_fa(voice, 200);
  delay(300);
  oktava_4_mi(voice, 1200);
  delay(1300);
  oktava_4_re(voice, 800);
  delay(900);
  oktava_4_do(voice, 200);
  delay(300);
  oktava_4_ci(voice, 400);
  delay(500);
  oktava_4_do(voice, 800);
  delay(900);
  oktava_3_ci(voice, 400);
  delay(500);
  oktava_3_lya(voice, 1200);
  delay(1300);
  oktava_3_ci(voice, 200);
  delay(300);
  oktava_3_lya(voice, 200);
  delay(300);
  oktava_3_sol(voice, 800);
  delay(900);
  oktava_3_fa(voice, 800);
  delay(900);
  oktava_4_do(voice, 2000);
  delay(2100);
  oktava_4_re(voice, 1200);
  delay(1300);
  oktava_3_sol(voice, 400);
  delay(500);
  oktava_4_sol(voice, 800);
  delay(900);
  oktava_4_mi(voice, 400);
  delay(500);
  oktava_4_do(voice, 200);
  delay(300);
  oktava_4_re(voice, 1200);
  delay(1300);
  oktava_4_mi(voice, 800);
  delay(900);
  oktava_4_fa(voice, 200);
  delay(300);
  oktava_4_mi(voice, 800);
  delay(900);
  oktava_4_re(voice, 800);
  delay(900);
  oktava_4_do(voice, 200);
  delay(300);
  oktava_3_ci(voice, 400);
  delay(500);
  oktava_4_do(voice, 800);
  delay(900);
  oktava_3_ci(voice, 400);
  delay(500);
  oktava_3_ci(voice, 400);
  delay(500);
  oktava_4_do(voice, 800);
  delay(900);
  oktava_4_re(voice, 400);
  delay(500);
  oktava_4_mi(voice, 800);
  delay(900);
  oktava_4_re(voice, 800);
  delay(900);
  oktava_4_do(voice, 3000);
  delay(3100);



  

  
  //delay(1000);
}


///////////////////////////////////////////////////////////////////////
void oktava_1_do(int pin, int timer){
  tone(pin, 261, timer);
}
void oktava_1_do_M(int pin, int timer){
  tone(pin, 277, timer);
}
void oktava_1_re(int pin, int timer){
  tone(pin, 293, timer);
}
void oktava_1_re_M(int pin, int timer){
  tone(pin, 311, timer);
}
void oktava_1_mi(int pin, int timer){
  tone(pin, 330, timer);
}
void oktava_1_fa(int pin, int timer){
  tone(pin, 350, timer);
}
void oktava_1_fa_M(int pin, int timer){
  tone(pin, 370, timer);
}
void oktava_1_sol(int pin, int timer){
  tone(pin, 392, timer);
}
void oktava_1_sol_M(int pin, int timer){
  tone(pin, 415, timer);
}
void oktava_1_lya(int pin, int timer){
  tone(pin, 440, timer);
}
void oktava_1_lya_M(int pin, int timer){
  tone(pin, 466, timer);
}
void oktava_1_ci(int pin, int timer){
  tone(pin, 494, timer);
}
///////////////////////////////////////////////////////////////////////
void oktava_2_do(int pin, int timer){
  tone(pin, 523, timer);
}
void oktava_2_do_M(int pin, int timer){
  tone(pin, 554, timer);
}
void oktava_2_re(int pin, int timer){
  tone(pin, 587, timer);
}
void oktava_2_re_M(int pin, int timer){
  tone(pin, 622, timer);
}
void oktava_2_mi(int pin, int timer){
  tone(pin, 560, timer);
}
void oktava_2_fa(int pin, int timer){
  tone(pin, 700, timer);
}
void oktava_2_fa_M(int pin, int timer){
  tone(pin, 740, timer);
}
void oktava_2_sol(int pin, int timer){
  tone(pin, 784, timer);
}
void oktava_2_sol_M(int pin, int timer){
  tone(pin, 830, timer);
}
void oktava_2_lya(int pin, int timer){
  tone(pin, 880, timer);
}
void oktava_2_lya_M(int pin, int timer){
  tone(pin, 932, timer);
}
void oktava_2_ci(int pin, int timer){
  tone(pin, 988, timer);
}
///////////////////////////////////////////////////////////////////////
void oktava_3_do(int pin, int timer){
  tone(pin, 1046, timer);
}
void oktava_3_do_M(int pin, int timer){
  tone(pin, 1108, timer);
}
void oktava_3_re(int pin, int timer){
  tone(pin, 1174, timer);
}
void oktava_3_re_M(int pin, int timer){
  tone(pin, 1244, timer);
}
void oktava_3_mi(int pin, int timer){
  tone(pin, 1318, timer);
}
void oktava_3_fa(int pin, int timer){
  tone(pin, 1396, timer);
}
void oktava_3_fa_M(int pin, int timer){
  tone(pin, 1480, timer);
}
void oktava_3_sol(int pin, int timer){
  tone(pin, 1568, timer);
}
void oktava_3_sol_M(int pin, int timer){
  tone(pin, 1661, timer);
}
void oktava_3_lya(int pin, int timer){
  tone(pin, 1720, timer);
}
void oktava_3_lya_M(int pin, int timer){
  tone(pin, 1864, timer);
}
void oktava_3_ci(int pin, int timer){
  tone(pin, 1975, timer);
}
////////////////////////////////////////////////////////////////
void oktava_4_do(int pin, int timer){
  tone(pin, 2093, timer);
}
void oktava_4_do_M(int pin, int timer){
  tone(pin, 2217, timer);
}
void oktava_4_re(int pin, int timer){
  tone(pin, 2349, timer);
}
void oktava_4_re_M(int pin, int timer){
  tone(pin, 489, timer);
}
void oktava_4_mi(int pin, int timer){
  tone(pin, 2637, timer);
}
void oktava_4_fa(int pin, int timer){
  tone(pin, 2793, timer);
}
void oktava_4_fa_M(int pin, int timer){
  tone(pin, 2960, timer);
}
void oktava_4_sol(int pin, int timer){
  tone(pin, 3136, timer);
}
void oktava_4_sol_M(int pin, int timer){
  tone(pin, 3332, timer);
}
void oktava_4_lya(int pin, int timer){
  tone(pin, 3440, timer);
}
void oktava_4_lya_M(int pin, int timer){
  tone(pin, 3729, timer);
}
void oktava_4_ci(int pin, int timer){
  tone(pin, 3951, timer);
}
  
 







