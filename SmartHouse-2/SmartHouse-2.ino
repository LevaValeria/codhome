#include <SoftwareSerial.h>

//pins
#define PIN_WATER A0
#define GROW A2 //Anya 
#define VOLUME 3 //Sasha
#define VOICE 4
#define LED_KITCHEN 9
#define LED_HOLL 10
#define LED_BATHROOM 11
#define LED_LIVINGROOM 12




int pinDoor = 2; //Lera//////////////////////////////////////

//qrow
const int minGrow = 200;
const int maxGrow = 700;
int countGrow = 0;
int countGrow2 = 0;

//Water
int maxWater = 800;

bool checkVolume = false;     //Sasha
bool checkDoor = false;
bool check = false; //
int readVolume = 0;
int portState = 0;

//card
SoftwareSerial rfid(7, 8); // RX, TX
boolean alarmOn = false;
boolean pos = false;
int readVol = 0;
int command = 0;

void setup() {


 
  pinMode(A2, INPUT);
  Serial.begin(9600);
  pinMode(PIN_WATER, INPUT);
  rfid.begin(9600);
  pinMode(LED_KITCHEN, OUTPUT);
  pinMode(LED_HOLL, OUTPUT);
  pinMode(LED_LIVINGROOM, OUTPUT);
  pinMode(LED_BATHROOM, OUTPUT);


  
}

void controlCard();
void cardAccess(int n);
void playMusic();
void moveMotionSensor();
void outputMotionSensor();
void checkoutDoor();
void floodWater();
void waterGrow();
void soundTitanic();
void oktava_1_do(int pin, int timer) {
  tone(pin, 261, timer);
}
void oktava_1_do_M(int pin, int timer) {
  tone(pin, 277, timer);
}
void oktava_1_re(int pin, int timer) {
  tone(pin, 293, timer);
}
void oktava_1_re_M(int pin, int timer) {
  tone(pin, 311, timer);
}
void oktava_1_mi(int pin, int timer) {
  tone(pin, 330, timer);
}
void oktava_1_fa(int pin, int timer) {
  tone(pin, 350, timer);
}
void oktava_1_fa_M(int pin, int timer) {
  tone(pin, 370, timer);
}
void oktava_1_sol(int pin, int timer) {
  tone(pin, 392, timer);
}
void oktava_1_sol_M(int pin, int timer) {
  tone(pin, 415, timer);
}
void oktava_1_lya(int pin, int timer) {
  tone(pin, 440, timer);
}
void oktava_1_lya_M(int pin, int timer) {
  tone(pin, 466, timer);
}
void oktava_1_ci(int pin, int timer) {
  tone(pin, 494, timer);
}
///////////////////////////////////////////////////////////////////////
void oktava_2_do(int pin, int timer) {
  tone(pin, 523, timer);
}
void oktava_2_do_M(int pin, int timer) {
  tone(pin, 554, timer);
}
void oktava_2_re(int pin, int timer) {
  tone(pin, 587, timer);
}
void oktava_2_re_M(int pin, int timer) {
  tone(pin, 622, timer);
}
void oktava_2_mi(int pin, int timer) {
  tone(pin, 560, timer);
}
void oktava_2_fa(int pin, int timer) {
  tone(pin, 700, timer);
}
void oktava_2_fa_M(int pin, int timer) {
  tone(pin, 740, timer);
}
void oktava_2_sol(int pin, int timer) {
  tone(pin, 784, timer);
}
void oktava_2_sol_M(int pin, int timer) {
  tone(pin, 830, timer);
}
void oktava_2_lya(int pin, int timer) {
  tone(pin, 880, timer);
}
void oktava_2_lya_M(int pin, int timer) {
  tone(pin, 932, timer);
}
void oktava_2_ci(int pin, int timer) {
  tone(pin, 988, timer);
}
///////////////////////////////////////////////////////////////////////
void oktava_3_do(int pin, int timer) {
  tone(pin, 1046, timer);
}
void oktava_3_do_M(int pin, int timer) {
  tone(pin, 1108, timer);
}
void oktava_3_re(int pin, int timer) {
  tone(pin, 1174, timer);
}
void oktava_3_re_M(int pin, int timer) {
  tone(pin, 1244, timer);
}
void oktava_3_mi(int pin, int timer) {
  tone(pin, 1318, timer);
}
void oktava_3_fa(int pin, int timer) {
  tone(pin, 1396, timer);
}
void oktava_3_fa_M(int pin, int timer) {
  tone(pin, 1480, timer);
}
void oktava_3_sol(int pin, int timer) {
  tone(pin, 1568, timer);
}
void oktava_3_sol_M(int pin, int timer) {
  tone(pin, 1661, timer);
}
void oktava_3_lya(int pin, int timer) {
  tone(pin, 1720, timer);
}
void oktava_3_lya_M(int pin, int timer) {
  tone(pin, 1864, timer);
}
void oktava_3_ci(int pin, int timer) {
  tone(pin, 1975, timer);
}
////////////////////////////////////////////////////////////////
void oktava_4_do(int pin, int timer) {
  tone(pin, 2093, timer);
}
void oktava_4_do_M(int pin, int timer) {
  tone(pin, 2217, timer);
}
void oktava_4_re(int pin, int timer) {
  tone(pin, 2349, timer);
}
void oktava_4_re_M(int pin, int timer) {
  tone(pin, 489, timer);
}
void oktava_4_mi(int pin, int timer) {
  tone(pin, 2637, timer);
}
void oktava_4_fa(int pin, int timer) {
  tone(pin, 2793, timer);
}
void oktava_4_fa_M(int pin, int timer) {
  tone(pin, 2960, timer);
}
void oktava_4_sol(int pin, int timer) {
  tone(pin, 3136, timer);
}
void oktava_4_sol_M(int pin, int timer) {
  tone(pin, 3332, timer);
}
void oktava_4_lya(int pin, int timer) {
  tone(pin, 3440, timer);
}
void oktava_4_lya_M(int pin, int timer) {
  tone(pin, 3729, timer);
}
void oktava_4_ci(int pin, int timer) {
  tone(pin, 3951, timer);
}

void loop() {
  


  

  floodWater ();
  checkoutDoor ();
  moveMotionSensor();
  waterGrow();
  controlCard();
 
  

}
void OutputAlarm() {
  Serial.println("ALARM");
}

void controlCard()
{
  portState = Serial.read();
  

if (portState == 'A') {
    digitalWrite(LED_KITCHEN, HIGH);
  }
  else if (portState == 'Z') {
    digitalWrite(LED_KITCHEN, LOW);
  }


  else if (portState == 'S') {
    digitalWrite(LED_HOLL, HIGH);
  }
  else if (portState == 'X') {
    digitalWrite(LED_HOLL, LOW);
  }

  else if (portState == 'D') {
    digitalWrite(LED_BATHROOM, HIGH);
  }
  else if (portState == 'C') {
    digitalWrite(LED_BATHROOM, LOW);
  }

  else if (portState == 'G') {
    digitalWrite(LED_LIVINGROOM, HIGH);
  }
  else if (portState == 'B') {
    digitalWrite(LED_LIVINGROOM, LOW);
  }

  command = portState;
  if (command == 'F' || command == '0') {
    alarmOn = false;
    Serial.println("Deactivated by port");

  }

  if (command == 'T' || command == '1') {
    alarmOn = true;
    Serial.println("Activated by port");}



  
  
  if (rfid.available())
  {
    if (rfid.find("74002738A5CE")) cardAccess(1);
  }
  if (alarmOn)readVol = digitalRead(VOLUME);
  if (readVol > 0 && !pos && alarmOn)
  {
    OutputAlarm();
    pos = true;

  }
  else if (readVol == 0 && pos) {
    pos = false;

  }
  if (pos && alarmOn)
    playMusic();}

void cardAccess(int n) {
  Serial.print("Card read : ");
  Serial.println(n);
  if (alarmOn)
  {
    Serial.println("Deactivated by card");
    Serial.println("od");
    alarmOn = false;
    tone(VOICE, 500);
    delay(100);
    tone(VOICE, 1000);
    delay(100);
    noTone(VOICE);
    analogWrite (LED_KITCHEN, 0);
    analogWrite (LED_HOLL, 0);
    analogWrite (LED_BATHROOM, 0);
    analogWrite (LED_LIVINGROOM, 0);
  }
  else
  {
    Serial.println("Activated");
    Serial.println("oa");
    alarmOn = true;
    tone(VOICE, 1000);
    delay(100);
    tone(VOICE, 500);
    delay(100);
    noTone(VOICE);
    delay(1500);
  }

}
void playMusic() {
  tone(VOICE, 1000);
  analogWrite (LED_KITCHEN, 255);
  analogWrite (LED_HOLL, 255);
  analogWrite (LED_BATHROOM, 255);
  analogWrite (LED_LIVINGROOM, 255);
  delay(100);
  tone(VOICE, 4600);
  analogWrite (LED_KITCHEN, 0);
  analogWrite (LED_HOLL, 0);
  analogWrite (LED_BATHROOM, 0);
  analogWrite (LED_LIVINGROOM, 0);
  delay(100);
  tone(VOICE, 900);
  analogWrite (LED_KITCHEN, 255);
  analogWrite (LED_HOLL, 255);
  analogWrite (LED_BATHROOM, 255);
  analogWrite (LED_LIVINGROOM, 255);
  delay(100);
  analogWrite (LED_KITCHEN, 0);
  analogWrite (LED_HOLL, 0);
  analogWrite (LED_BATHROOM, 0);
  analogWrite (LED_LIVINGROOM, 0);
  noTone(VOICE);
}


void moveMotionSensor() {
  readVolume = digitalRead(VOLUME);
  if (readVolume > 0 && !checkVolume) {
    outputMotionSensor();
    checkVolume = true;
  }
  else if (readVolume == 0 && checkVolume) {
    checkVolume = false;
  }
}

void outputMotionSensor() {
  Serial.println("dvk");
}

void checkoutDoor () {
  int valDoor = digitalRead(pinDoor);

  if (valDoor == HIGH && !checkDoor) {
    checkDoor = true;
    Serial.println("dc");
  }
  else if (valDoor == LOW && checkDoor) {
    checkDoor = false;
    Serial.println("do");
  }
}

void floodWater() {
  int W_val = analogRead(PIN_WATER);
  if (W_val < maxWater && !check) {
    Serial.println("pvv");
    check = true;
    soundTitanic();
  }
  else if (W_val > maxWater && (check)) {
    check = false;
  }
}

void waterGrow() {
  int valGrow = analogRead(GROW);
  if (valGrow <= minGrow && countGrow2 == 1)
  {
    Serial.println("dvn");
    countGrow2 = 0;
  }
  else if (valGrow >= maxGrow && countGrow == 0 && countGrow2 == 0) {
    Serial.println("dvc");
    countGrow = 1;
    countGrow2 = 1;
  }
  else if ((valGrow >= minGrow) && (valGrow <= maxGrow) && countGrow == 1) {
    Serial.println("dvn");
    countGrow = 0;
  }
}


void soundTitanic() {
  if (rfid.find("74002738A5CE")) {
    noTone(oktava_4_do);
    noTone(oktava_4_re);
    noTone(oktava_4_mi);
    noTone(oktava_4_fa);
    noTone(oktava_4_sol);
    noTone(oktava_4_ci);
    noTone(oktava_3_ci);
    noTone(oktava_3_do);
    noTone(oktava_3_re);
    noTone(oktava_3_mi);
    noTone(oktava_3_fa);
    noTone(oktava_3_sol);
    noTone(oktava_3_lya);
  }
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);//800
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_re(VOICE, 400);//400
  delay(500);
  oktava_4_mi(VOICE, 900);
  delay(1000);
  oktava_4_re(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_sol(VOICE, 2000);
  delay(2100);


  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);//800
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_re(VOICE, 400);
  delay(500);
  oktava_4_mi(VOICE, 800);
  delay(900);
  oktava_4_re(VOICE, 800);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_sol(VOICE, 3000);
  delay(3100);


  oktava_3_lya(VOICE, 800);
  delay(900);
  oktava_3_ci(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 2000);
  delay(2100);
  oktava_4_re(VOICE, 1200);
  delay(1300);
  oktava_3_sol(VOICE, 400);
  delay(500);
  oktava_4_sol(VOICE, 800);
  delay(900);
  oktava_4_fa(VOICE, 400);
  delay(500);
  oktava_4_mi(VOICE, 200);
  delay(300);
  oktava_4_re(VOICE, 1200);
  delay(1300);
  oktava_4_mi(VOICE, 800);
  delay(900);
  oktava_4_fa(VOICE, 200);
  delay(300);
  oktava_4_mi(VOICE, 1200);
  delay(1300);
  oktava_4_re(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_4_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_3_lya(VOICE, 1200);
  delay(1300);
  oktava_3_ci(VOICE, 200);
  delay(300);
  oktava_3_lya(VOICE, 200);
  delay(300);
  oktava_3_sol(VOICE, 800);
  delay(900);
  oktava_3_fa(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 2000);
  delay(2100);
  oktava_4_re(VOICE, 1200);
  delay(1300);
  oktava_3_sol(VOICE, 400);
  delay(500);
  oktava_4_sol(VOICE, 800);
  delay(900);
  oktava_4_mi(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_4_re(VOICE, 1200);
  delay(1300);
  oktava_4_mi(VOICE, 800);
  delay(900);
  oktava_4_fa(VOICE, 200);
  delay(300);
  oktava_4_mi(VOICE, 800);
  delay(900);
  oktava_4_re(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_re(VOICE, 400);
  delay(500);
  oktava_4_mi(VOICE, 800);
  delay(900);
  oktava_4_re(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 3000);
  delay(3100);






  //delay(1000);
}


///////////////////////////////////////////////////////////////////////








#include <SoftwareSerial.h>

//pins
#define PIN_WATER A0
#define GROW A2 //Anya 
#define VOLUME 3 //Sasha
#define VOICE 4
#define LED_KITCHEN 9
#define LED_HOLL 10
#define LED_BATHROOM 11
#define LED_LIVINGROOM 12




int pinDoor = 2; //Lera//////////////////////////////////////

//qrow
const int minGrow = 200;
const int maxGrow = 700;
int countGrow = 0;
int countGrow2 = 0;

//Water
int maxWater = 800;

bool checkVolume = false;     //Sasha
bool checkDoor = false;
bool check = false; //
int readVolume = 0;
int portState = 0;

//card
SoftwareSerial rfid(7, 8); // RX, TX
boolean alarmOn = false;
boolean pos = false;
int readVol = 0;
int command = 0;

void setup() {


 
  pinMode(A2, INPUT);
  Serial.begin(9600);
  pinMode(PIN_WATER, INPUT);
  rfid.begin(9600);
  pinMode(LED_KITCHEN, OUTPUT);
  pinMode(LED_HOLL, OUTPUT);
  pinMode(LED_LIVINGROOM, OUTPUT);
  pinMode(LED_BATHROOM, OUTPUT);


  
}

void controlCard();
void cardAccess(int n);
void playMusic();
void moveMotionSensor();
void outputMotionSensor();
void checkoutDoor();
void floodWater();
void waterGrow();
void soundTitanic();
void oktava_1_do(int pin, int timer) {
  tone(pin, 261, timer);
}
void oktava_1_do_M(int pin, int timer) {
  tone(pin, 277, timer);
}
void oktava_1_re(int pin, int timer) {
  tone(pin, 293, timer);
}
void oktava_1_re_M(int pin, int timer) {
  tone(pin, 311, timer);
}
void oktava_1_mi(int pin, int timer) {
  tone(pin, 330, timer);
}
void oktava_1_fa(int pin, int timer) {
  tone(pin, 350, timer);
}
void oktava_1_fa_M(int pin, int timer) {
  tone(pin, 370, timer);
}
void oktava_1_sol(int pin, int timer) {
  tone(pin, 392, timer);
}
void oktava_1_sol_M(int pin, int timer) {
  tone(pin, 415, timer);
}
void oktava_1_lya(int pin, int timer) {
  tone(pin, 440, timer);
}
void oktava_1_lya_M(int pin, int timer) {
  tone(pin, 466, timer);
}
void oktava_1_ci(int pin, int timer) {
  tone(pin, 494, timer);
}
///////////////////////////////////////////////////////////////////////
void oktava_2_do(int pin, int timer) {
  tone(pin, 523, timer);
}
void oktava_2_do_M(int pin, int timer) {
  tone(pin, 554, timer);
}
void oktava_2_re(int pin, int timer) {
  tone(pin, 587, timer);
}
void oktava_2_re_M(int pin, int timer) {
  tone(pin, 622, timer);
}
void oktava_2_mi(int pin, int timer) {
  tone(pin, 560, timer);
}
void oktava_2_fa(int pin, int timer) {
  tone(pin, 700, timer);
}
void oktava_2_fa_M(int pin, int timer) {
  tone(pin, 740, timer);
}
void oktava_2_sol(int pin, int timer) {
  tone(pin, 784, timer);
}
void oktava_2_sol_M(int pin, int timer) {
  tone(pin, 830, timer);
}
void oktava_2_lya(int pin, int timer) {
  tone(pin, 880, timer);
}
void oktava_2_lya_M(int pin, int timer) {
  tone(pin, 932, timer);
}
void oktava_2_ci(int pin, int timer) {
  tone(pin, 988, timer);
}
///////////////////////////////////////////////////////////////////////
void oktava_3_do(int pin, int timer) {
  tone(pin, 1046, timer);
}
void oktava_3_do_M(int pin, int timer) {
  tone(pin, 1108, timer);
}
void oktava_3_re(int pin, int timer) {
  tone(pin, 1174, timer);
}
void oktava_3_re_M(int pin, int timer) {
  tone(pin, 1244, timer);
}
void oktava_3_mi(int pin, int timer) {
  tone(pin, 1318, timer);
}
void oktava_3_fa(int pin, int timer) {
  tone(pin, 1396, timer);
}
void oktava_3_fa_M(int pin, int timer) {
  tone(pin, 1480, timer);
}
void oktava_3_sol(int pin, int timer) {
  tone(pin, 1568, timer);
}
void oktava_3_sol_M(int pin, int timer) {
  tone(pin, 1661, timer);
}
void oktava_3_lya(int pin, int timer) {
  tone(pin, 1720, timer);
}
void oktava_3_lya_M(int pin, int timer) {
  tone(pin, 1864, timer);
}
void oktava_3_ci(int pin, int timer) {
  tone(pin, 1975, timer);
}
////////////////////////////////////////////////////////////////
void oktava_4_do(int pin, int timer) {
  tone(pin, 2093, timer);
}
void oktava_4_do_M(int pin, int timer) {
  tone(pin, 2217, timer);
}
void oktava_4_re(int pin, int timer) {
  tone(pin, 2349, timer);
}
void oktava_4_re_M(int pin, int timer) {
  tone(pin, 489, timer);
}
void oktava_4_mi(int pin, int timer) {
  tone(pin, 2637, timer);
}
void oktava_4_fa(int pin, int timer) {
  tone(pin, 2793, timer);
}
void oktava_4_fa_M(int pin, int timer) {
  tone(pin, 2960, timer);
}
void oktava_4_sol(int pin, int timer) {
  tone(pin, 3136, timer);
}
void oktava_4_sol_M(int pin, int timer) {
  tone(pin, 3332, timer);
}
void oktava_4_lya(int pin, int timer) {
  tone(pin, 3440, timer);
}
void oktava_4_lya_M(int pin, int timer) {
  tone(pin, 3729, timer);
}
void oktava_4_ci(int pin, int timer) {
  tone(pin, 3951, timer);
}

void loop() {
  


  

  floodWater ();
  checkoutDoor ();
  moveMotionSensor();
  waterGrow();
  controlCard();
 
  

}
void OutputAlarm() {
  Serial.println("ALARM");
}

void controlCard()
{
  portState = Serial.read();
  

if (portState == 'A') {
    digitalWrite(LED_KITCHEN, HIGH);
  }
  else if (portState == 'Z') {
    digitalWrite(LED_KITCHEN, LOW);
  }


  else if (portState == 'S') {
    digitalWrite(LED_HOLL, HIGH);
  }
  else if (portState == 'X') {
    digitalWrite(LED_HOLL, LOW);
  }

  else if (portState == 'D') {
    digitalWrite(LED_BATHROOM, HIGH);
  }
  else if (portState == 'C') {
    digitalWrite(LED_BATHROOM, LOW);
  }

  else if (portState == 'G') {
    digitalWrite(LED_LIVINGROOM, HIGH);
  }
  else if (portState == 'B') {
    digitalWrite(LED_LIVINGROOM, LOW);
  }

  command = portState;
  if (command == 'F' || command == '0') {
    alarmOn = false;
    Serial.println("Deactivated by port");

  }

  if (command == 'T' || command == '1') {
    alarmOn = true;
    Serial.println("Activated by port");}



  
  
  if (rfid.available())
  {
    if (rfid.find("74002738A5CE")) cardAccess(1);
  }
  if (alarmOn)readVol = digitalRead(VOLUME);
  if (readVol > 0 && !pos && alarmOn)
  {
    OutputAlarm();
    pos = true;

  }
  else if (readVol == 0 && pos) {
    pos = false;

  }
  if (pos && alarmOn)
    playMusic();}

void cardAccess(int n) {
  Serial.print("Card read : ");
  Serial.println(n);
  if (alarmOn)
  {
    Serial.println("Deactivated by card");
    Serial.println("od");
    alarmOn = false;
    tone(VOICE, 500);
    delay(100);
    tone(VOICE, 1000);
    delay(100);
    noTone(VOICE);
    analogWrite (LED_KITCHEN, 0);
    analogWrite (LED_HOLL, 0);
    analogWrite (LED_BATHROOM, 0);
    analogWrite (LED_LIVINGROOM, 0);
  }
  else
  {
    Serial.println("Activated");
    Serial.println("oa");
    alarmOn = true;
    tone(VOICE, 1000);
    delay(100);
    tone(VOICE, 500);
    delay(100);
    noTone(VOICE);
    delay(1500);
  }

}
void playMusic() {
  tone(VOICE, 1000);
  analogWrite (LED_KITCHEN, 255);
  analogWrite (LED_HOLL, 255);
  analogWrite (LED_BATHROOM, 255);
  analogWrite (LED_LIVINGROOM, 255);
  delay(100);
  tone(VOICE, 4600);
  analogWrite (LED_KITCHEN, 0);
  analogWrite (LED_HOLL, 0);
  analogWrite (LED_BATHROOM, 0);
  analogWrite (LED_LIVINGROOM, 0);
  delay(100);
  tone(VOICE, 900);
  analogWrite (LED_KITCHEN, 255);
  analogWrite (LED_HOLL, 255);
  analogWrite (LED_BATHROOM, 255);
  analogWrite (LED_LIVINGROOM, 255);
  delay(100);
  analogWrite (LED_KITCHEN, 0);
  analogWrite (LED_HOLL, 0);
  analogWrite (LED_BATHROOM, 0);
  analogWrite (LED_LIVINGROOM, 0);
  noTone(VOICE);
}


void moveMotionSensor() {
  readVolume = digitalRead(VOLUME);
  if (readVolume > 0 && !checkVolume) {
    outputMotionSensor();
    checkVolume = true;
  }
  else if (readVolume == 0 && checkVolume) {
    checkVolume = false;
  }
}

void outputMotionSensor() {
  Serial.println("dvk");
}

void checkoutDoor () {
  int valDoor = digitalRead(pinDoor);

  if (valDoor == HIGH && !checkDoor) {
    checkDoor = true;
    Serial.println("dc");
  }
  else if (valDoor == LOW && checkDoor) {
    checkDoor = false;
    Serial.println("do");
  }
}

void floodWater() {
  int W_val = analogRead(PIN_WATER);
  if (W_val < maxWater && !check) {
    Serial.println("pvv");
    check = true;
    soundTitanic();
  }
  else if (W_val > maxWater && (check)) {
    check = false;
  }
}

void waterGrow() {
  int valGrow = analogRead(GROW);
  if (valGrow <= minGrow && countGrow2 == 1)
  {
    Serial.println("dvn");
    countGrow2 = 0;
  }
  else if (valGrow >= maxGrow && countGrow == 0 && countGrow2 == 0) {
    Serial.println("dvc");
    countGrow = 1;
    countGrow2 = 1;
  }
  else if ((valGrow >= minGrow) && (valGrow <= maxGrow) && countGrow == 1) {
    Serial.println("dvn");
    countGrow = 0;
  }
}


void soundTitanic() {
  if (rfid.find("74002738A5CE")) {
    noTone(oktava_4_do);
    noTone(oktava_4_re);
    noTone(oktava_4_mi);
    noTone(oktava_4_fa);
    noTone(oktava_4_sol);
    noTone(oktava_4_ci);
    noTone(oktava_3_ci);
    noTone(oktava_3_do);
    noTone(oktava_3_re);
    noTone(oktava_3_mi);
    noTone(oktava_3_fa);
    noTone(oktava_3_sol);
    noTone(oktava_3_lya);
  }
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);//800
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_re(VOICE, 400);//400
  delay(500);
  oktava_4_mi(VOICE, 900);
  delay(1000);
  oktava_4_re(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_sol(VOICE, 2000);
  delay(2100);


  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);//800
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_re(VOICE, 400);
  delay(500);
  oktava_4_mi(VOICE, 800);
  delay(900);
  oktava_4_re(VOICE, 800);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 400);
  delay(500);
  oktava_3_sol(VOICE, 3000);
  delay(3100);


  oktava_3_lya(VOICE, 800);
  delay(900);
  oktava_3_ci(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 2000);
  delay(2100);
  oktava_4_re(VOICE, 1200);
  delay(1300);
  oktava_3_sol(VOICE, 400);
  delay(500);
  oktava_4_sol(VOICE, 800);
  delay(900);
  oktava_4_fa(VOICE, 400);
  delay(500);
  oktava_4_mi(VOICE, 200);
  delay(300);
  oktava_4_re(VOICE, 1200);
  delay(1300);
  oktava_4_mi(VOICE, 800);
  delay(900);
  oktava_4_fa(VOICE, 200);
  delay(300);
  oktava_4_mi(VOICE, 1200);
  delay(1300);
  oktava_4_re(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_4_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_3_lya(VOICE, 1200);
  delay(1300);
  oktava_3_ci(VOICE, 200);
  delay(300);
  oktava_3_lya(VOICE, 200);
  delay(300);
  oktava_3_sol(VOICE, 800);
  delay(900);
  oktava_3_fa(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 2000);
  delay(2100);
  oktava_4_re(VOICE, 1200);
  delay(1300);
  oktava_3_sol(VOICE, 400);
  delay(500);
  oktava_4_sol(VOICE, 800);
  delay(900);
  oktava_4_mi(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_4_re(VOICE, 1200);
  delay(1300);
  oktava_4_mi(VOICE, 800);
  delay(900);
  oktava_4_fa(VOICE, 200);
  delay(300);
  oktava_4_mi(VOICE, 800);
  delay(900);
  oktava_4_re(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 200);
  delay(300);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_3_ci(VOICE, 400);
  delay(500);
  oktava_4_do(VOICE, 800);
  delay(900);
  oktava_4_re(VOICE, 400);
  delay(500);
  oktava_4_mi(VOICE, 800);
  delay(900);
  oktava_4_re(VOICE, 800);
  delay(900);
  oktava_4_do(VOICE, 3000);
  delay(3100);






  //delay(1000);
}


///////////////////////////////////////////////////////////////////////









